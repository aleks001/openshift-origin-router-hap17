# what's this

This repo uses haproxy 1.7 as openshift router.

It ist based on openshift/origin-haproxy-router from docker hub.

# how to build

## on openshift

```
oc new-app https://gitlab.com/aleks001/openshift-origin-router-hap17.git
oc logs -f bc/openshift-origin-router-hap17
```

wait until the push is successully.

```
oc logs -f openshift-origin-router-hap17-1-efgqz
I0302 12:25:49.519595       1 router.go:236] Router is including routes in all namespaces
```

## with docker
```
docker build --tag me2digital/openshift-origin-router-hap17 https://gitlab.com/aleks001/openshift-origin-router-hap17.git
```

# various output

## ps

```
oc rsh openshift-origin-router-hap17-1-efgqz
sh-4.2$ ps axfww

   PID TTY      STAT   TIME COMMAND

    35 ?        Ss     0:00 /bin/sh

    42 ?        R+     0:00  \_ ps axfww

     1 ?        Ssl    0:00 /usr/bin/openshift-router

```

## version

```
sh-4.2$ /usr/bin/openshift-router version  
openshift-router v3.6.0-rc.0+198b66f-252
kubernetes v1.7.0+695f48a16f
```

## haproxy -vv 
```
HA-Proxy version 1.7.8 2017/07/07
Copyright 2000-2017 Willy Tarreau <willy@haproxy.org>

Build options :
  TARGET  = linux2628
  CPU     = generic
  CC      = gcc
  CFLAGS  = -O2 -g -fno-strict-aliasing -Wdeclaration-after-statement -fwrapv
  OPTIONS = USE_LINUX_SPLICE=1 USE_GETADDRINFO=1 USE_ZLIB=1 USE_REGPARM=1 USE_OPENSSL=1 USE_LUA=1 USE_PCRE=1 USE_PCRE_JIT=1 USE_TFO=1

Default settings :
  maxconn = 2000, bufsize = 16384, maxrewrite = 1024, maxpollevents = 200

Encrypted password support via crypt(3): yes
Built with zlib version : 1.2.7
Running on zlib version : 1.2.7
Compression algorithms supported : identity("identity"), deflate("deflate"), raw-deflate("deflate"), gzip("gzip")
Built with OpenSSL version : OpenSSL 1.0.1e-fips 11 Feb 2013
Running on OpenSSL version : OpenSSL 1.0.1e-fips 11 Feb 2013
OpenSSL library supports TLS extensions : yes
OpenSSL library supports SNI : yes
OpenSSL library supports prefer-server-ciphers : yes
Built with PCRE version : 8.32 2012-11-30
Running on PCRE version : 8.32 2012-11-30
PCRE library supports JIT : yes
Built with Lua version : Lua 5.3.4
Built with transparent proxy support using: IP_TRANSPARENT IPV6_TRANSPARENT IP_FREEBIND

Available polling systems :
      epoll : pref=300,  test result OK
       poll : pref=200,  test result OK
     select : pref=150,  test result OK
Total: 3 (3 usable), will use epoll.

Available filters :
        [COMP] compression
        [TRACE] trace
        [SPOE] spoe

```